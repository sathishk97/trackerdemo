package com.divum.trackerdemo;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener,LocationListener {


    EditText TURL, DeviceID;
    private Double Latitude, Longitude;
    private LocationManager locationManager;
    private Switch mySwitch;
    private int PERMISSION_CODE = 100;
    Boolean send=false;
    int time =0;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RequestMultiplePermission();
        if(isNetworkAvailable());
        else{
            dialog("Please enable internet");
        }

        TURL = findViewById(R.id.txtURL);
        DeviceID = findViewById(R.id.txtdeviceID);
        mySwitch = (Switch) findViewById(R.id.SWStart);
        mySwitch.setOnCheckedChangeListener(this);

    }
    private void RequestMultiplePermission() {

        // Creating String Array with Permissions.
        ActivityCompat.requestPermissions(MainActivity.this, new String[]
                {
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.INTERNET

                }, PERMISSION_CODE);

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public void dialog(String msg){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg);
        builder.setCancelable(true);

        builder.setPositiveButton(
                "ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void setLocationManager() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            assert locationManager != null;
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 8000, 5, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void resetLocationManager() {
        locationManager.removeUpdates(this);
        locationManager = null;
    }

    public boolean isValid(){

        if(TextUtils.isEmpty(DeviceID.getText())) {
            DeviceID.setFocusable(true);
            DeviceID.setError("Please enter device ID");
            DeviceID.requestFocus();
            return false;
        }
        if(TextUtils.isEmpty(TURL.getText())){
            TURL.setFocusable(true);
            TURL.setError("Please enter URL");
            TURL.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
       if (isChecked) {
          if(isValid()){
              setLocationManager();
              send=true;
              TURL.setEnabled(false);
              DeviceID.setEnabled(false);
          }
          else mySwitch.setChecked(false);
        }
        else{
            resetLocationManager();
            TURL.setEnabled(true);
            DeviceID.setEnabled(true);
            send = false;
        }
    }
    public void sent_to_server(){
        Position position = new Position();
        position.setDeviceId(DeviceID.getText().toString());
        position.setLatitude(Latitude);
        position.setLongitude(Longitude);
        String request = ProtocolFormatter.formatRequest(TURL.getText().toString(), position);
        RequestManager.sendRequestAsync(request, new RequestManager.RequestHandler() {
            @Override
            public void onComplete(boolean success) {

            }
        });
    }

    @Override
    public void onLocationChanged(Location location) {
        Longitude = location.getLongitude();
        Latitude = location.getLatitude();
        setLocationManager();
        Log.d("check", "location");
        if(send){
            if(time%5==0){
                sent_to_server();
                Log.d("check", "send");
            }
            time++;
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_CODE) {
            if (grantResults.length > 0 ) {
                boolean Permission=false;
                for(int i = 0;i<permissions.length;i++){
                    Permission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                }
                if(Permission)
                    Toast.makeText(MainActivity.this,"Permission Granted", Toast.LENGTH_SHORT).show();
            } else {
                dialog("Permission Denied");
                Toast.makeText(MainActivity.this, " Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }



}