package com.divum.trackerdemo;

import android.location.Location;
import android.location.LocationManager;
import android.os.Build;

import java.util.Date;

public class Position {
    public Position() {

    }
    public Position(String deviceId) {
        this.deviceId = deviceId;
    }

    public Position( Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }



    private String deviceId;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }


    private double latitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    private double longitude;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


}